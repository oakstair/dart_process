import 'dart:io';

import 'package:dart_process/stockfish.dart';
import 'package:test/test.dart';

// Note: We are starting stockfish (in various ways) plus sending some commands to it. The expected output is
String expectedResult = '''
Stockfish 310320 64 by T. Romstad, M. Costalba, J. Kiiski, G. Linscott
readyok
id name Stockfish 310320 64
id author T. Romstad, M. Costalba, J. Kiiski, G. Linscott

option name Debug Log File type string default
option name Contempt type spin default 24 min -100 max 100
option name Analysis Contempt type combo default Both var Off var White var Black var Both
option name Threads type spin default 1 min 1 max 512
option name Hash type spin default 16 min 1 max 131072
option name Clear Hash type button
option name Ponder type check default false
option name MultiPV type spin default 1 min 1 max 500
option name Skill Level type spin default 20 min 0 max 20
option name Move Overhead type spin default 30 min 0 max 5000
option name Minimum Thinking Time type spin default 20 min 0 max 5000
option name Slow Mover type spin default 84 min 10 max 1000
option name nodestime type spin default 0 min 0 max 10000
option name UCI_Chess960 type check default false
option name UCI_AnalyseMode type check default false
option name UCI_LimitStrength type check default false
option name UCI_Elo type spin default 1350 min 1350 max 2850
option name SyzygyPath type string default <empty>
option name SyzygyProbeDepth type spin default 1 min 1 max 100
option name Syzygy50MoveRule type check default true
option name SyzygyProbeLimit type spin default 7 min 0 max 7
uciok
readyok
''';

typedef StartFunction = Function(OnLine onDataLine, OnLine onErrorLine);

void main() {
  test('startBinaryDirect', () async {

    Future<void> testStart(Stockfish sf, StartFunction start) async {
      DateTime startedAt = DateTime.now();

      int millis() => DateTime.now().difference(startedAt).inMilliseconds;
      await start((String line) => print('${millis()}: $line'),
          (String error) => print('${millis()}: ERROR: $error'));

      // -- Send some commands to stockfish. --
      sf.sendCommand('isready');
      sf.sendCommand('uci');
      sf.sendCommand('isready');

      // -- Give stockfish more than enough time to process the commands. --
      sleep(Duration(seconds: 2));
    }

    Stockfish sf1 = Stockfish();
    await testStart(sf1, sf1.startBinaryDirect_WithLineSplitting);
    print('Done startBinaryDirect_WithLineSplitting');

    Stockfish sf2 = Stockfish();
    await testStart(sf2, sf2.startBinaryDirect_JustRedirect);
    print('Done startBinaryDirect_JustRedirect');

    Stockfish sf3 = Stockfish();
    await testStart(sf3, sf3.startUsingStdbuf_WithLineSplitting);
    print('Done startUsingStdbuf_WithLineSplitting');

    sleep(Duration(seconds: 3));
  });
}
