import 'dart:convert';
import 'dart:io';

typedef OnLine = Function(String line);

/// Run stockfish as a remote process.
class Stockfish {

  Process _process;

  Stockfish();

  void startBinaryDirect_WithLineSplitting(OnLine handleReply, [OnLine handleError]) async {
    await Process.start('./stockfish', []).then((p) {
      _process = p;
      _process.stdout.transform(Utf8Decoder()).transform(LineSplitter())
          .listen(handleReply);
      _process.stderr.transform(Utf8Decoder()).transform(LineSplitter())
          .listen(handleError);
    });
  }

  void startBinaryDirect_JustRedirect(OnLine handleReply, [OnLine handleError]) async {
    await Process.start('./stockfish', []).then((p) {
      _process = p;
      stdout.addStream(_process.stdout);
      stderr.addStream(_process.stderr);
    });
  }

  void startUsingStdbuf_WithLineSplitting(OnLine handleReply, [OnLine handleError]) async {

    await Process.start('stdbuf', ['-oL', '-eL', './stockfish'], runInShell: true).then((p) {
      _process = p;
      _process.stdout.transform(Utf8Decoder()).transform(LineSplitter())
          .listen(handleReply);
      _process.stderr.transform(Utf8Decoder()).transform(LineSplitter())
          .listen(handleError);
    });
  }

  /// Stop engine and kill process.
  void stop() {
    _process.kill();
  }

  void sendCommand(String cmd) {
    _process.stdin.writeln(cmd);
  }

}
